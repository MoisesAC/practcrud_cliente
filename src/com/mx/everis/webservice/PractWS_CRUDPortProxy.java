package com.mx.everis.webservice;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;
import java.util.List;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;

public class PractWS_CRUDPortProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private com.mx.everis.webservice.PractWSCRUDService _service = null;
        private com.mx.everis.webservice.PractWSCRUDDelegate _proxy = null;
        private Dispatch<Source> _dispatch = null;
        private boolean _useJNDIOnly = false;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new com.mx.everis.webservice.PractWSCRUDService(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            try
            {
                InitialContext ctx = new InitialContext();
                _service = (com.mx.everis.webservice.PractWSCRUDService)ctx.lookup("java:comp/env/service/PractWS_CRUDService");
            }
            catch (NamingException e)
            {
                if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
                    System.out.println("JNDI lookup failure: javax.naming.NamingException: " + e.getMessage());
                    e.printStackTrace(System.out);
                }
            }

            if (_service == null && !_useJNDIOnly)
                _service = new com.mx.everis.webservice.PractWSCRUDService();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getPractWSCRUDPort();
        }

        public com.mx.everis.webservice.PractWSCRUDDelegate getProxy() {
            return _proxy;
        }

        public void useJNDIOnly(boolean useJNDIOnly) {
            _useJNDIOnly = useJNDIOnly;
            init();
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("", "PractWS_CRUDPort");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

        public void setMTOMEnabled(boolean enable) {
            SOAPBinding binding = (SOAPBinding) ((BindingProvider) _proxy).getBinding();
            binding.setMTOMEnabled(enable);
        }
    }

    public PractWS_CRUDPortProxy() {
        _descriptor = new Descriptor();
        _descriptor.setMTOMEnabled(false);
    }

    public PractWS_CRUDPortProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
        _descriptor.setMTOMEnabled(false);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public void agrega(int arg0, String arg1, String arg2, int arg3, String arg4, String arg5) throws ClassNotFoundException_Exception, SQLException_Exception {
        _getDescriptor().getProxy().agrega(arg0,arg1,arg2,arg3,arg4,arg5);
    }

    public void elimina(int arg0, String arg1) throws ClassNotFoundException_Exception, SQLException_Exception {
        _getDescriptor().getProxy().elimina(arg0,arg1);
    }

    public void modifica(int arg0, String arg1, String arg2, int arg3, String arg4, String arg5) throws ClassNotFoundException_Exception, SQLException_Exception {
        _getDescriptor().getProxy().modifica(arg0,arg1,arg2,arg3,arg4,arg5);
    }

    public List<Registro> consulta(int arg0) throws ClassNotFoundException_Exception, SQLException_Exception {
        return _getDescriptor().getProxy().consulta(arg0);
    }

}