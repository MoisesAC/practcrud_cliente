<%@page contentType="text/html;charset=UTF-8"%>
<%
request.setCharacterEncoding("UTF-8");
com.ibm.ccl.ws.jaxws.gstc.util.OutputUtils.init(session);

boolean async       = session.getAttribute("__async__") == null ? false : true;
String methodKey    = request.getParameter("key");
String resultSuffix = methodKey != null && methodKey.length() > 0 ? " - " + methodKey : "";
%>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
<LINK rel="stylesheet" type="text/css" href="TestClient.css"/>
<script language="JavaScript">

function reloadMethods() {
    window.parent.frames["methods"].location.reload(true);
}
</script>
</HEAD>
<BODY>
<TABLE class="heading" width="100%">
<TR><TD>Result<%= org.eclipse.jst.ws.util.JspUtils.markup(resultSuffix) %></TD></TR>
</TABLE>
<P>
<jsp:useBean id="samplePractWS_CRUDPortProxyid" scope="session" class="com.mx.everis.webservice.PractWS_CRUDPortProxy" />

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

boolean isDone = true;
try {
    String sourceInTemp = request.getParameter("__rawxml__");
        javax.xml.transform.Source sourceIn  = sourceInTemp != null ?
            new javax.xml.transform.stream.StreamSource(new java.io.ByteArrayInputStream(sourceInTemp.getBytes())) : null;
    javax.xml.transform.Source sourceOut = null;

    boolean bypass = (sourceIn != null);

switch (methodID){ 
case 2:
    gotMethod = true;
    com.mx.everis.webservice.PractWS_CRUDPortProxy.Descriptor returnp3mtemp = null;
    if (methodKey != null) {
        javax.xml.ws.Response resp = com.ibm.ccl.ws.jaxws.gstc.util.AsyncUtils.getResponse(session, methodKey);
        if (resp != null) {
            isDone = resp.isDone();
            if (!isDone)
                break;
            if (resp.get() != null) {
                Object o = resp.get();
                if (o instanceof javax.xml.transform.Source)
                    sourceOut = (javax.xml.transform.Source) o;
                else if (o instanceof com.mx.everis.webservice.PractWS_CRUDPortProxy.Descriptor)
                    returnp3mtemp = (com.mx.everis.webservice.PractWS_CRUDPortProxy.Descriptor) o;
            }
        }
    }
    else if (bypass) {
        javax.xml.ws.Dispatch dispatch = samplePractWS_CRUDPortProxyid._getDescriptor().getDispatch();

        if (request.getParameter("__use_soapaction__") != null)
            com.ibm.ccl.ws.jaxws.gstc.util.DispatchUtils.useSoapAction(dispatch, request.getParameter("__soapaction__"));
        else
            com.ibm.ccl.ws.jaxws.gstc.util.DispatchUtils.clearSoapAction(dispatch);

        if (!async)
            sourceOut = (javax.xml.transform.Source) dispatch.invoke(sourceIn);
        else {
            // async code omitted
            break;
        }
    } else {
        if (!async) {
        try {
            returnp3mtemp = samplePractWS_CRUDPortProxyid._getDescriptor();
            }catch(Exception exc){
                %>
                Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(exc.toString()) %>
                Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(exc.getMessage()) %>
                <%
break;
            }
        }
        else {
            // async code omitted
            break;
        }
    }
if (sourceOut != null) {
%>
    <TEXTAREA ROWS="8" COLs="45"><%= org.eclipse.jst.ws.util.JspUtils.markup(com.ibm.ccl.ws.jaxws.gstc.util.SourceUtils.transform(sourceOut)) %></TEXTAREA>
<%
}
else {
if (returnp3mtemp == null) {
%>
    null
<%
} else {
    %>
    <TABLE CLASS="tableform">
<TR>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">returnp:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">proxy:</TD>
<TD>
<%
if(returnp3mtemp != null){
com.mx.everis.webservice.PractWSCRUDDelegate typeproxy5 = returnp3mtemp.getProxy();
if(typeproxy5 != null){
        if(typeproxy5!= null){
        String tempproxy5 = typeproxy5.toString();
        %>
        <%=tempproxy5%>
        <%
        }}
else{
        %>
        <%= typeproxy5%>
        <%
}
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">dispatch:</TD>
<TD>
<%
if(returnp3mtemp != null){
javax.xml.ws.Dispatch typedispatch7 = returnp3mtemp.getDispatch();
if(typedispatch7 != null){
        if(typedispatch7!= null){
        String tempdispatch7 = typedispatch7.toString();
        %>
        <%=tempdispatch7%>
        <%
        }}
else{
        %>
        <%= typedispatch7%>
        <%
}
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">endpoint:</TD>
<TD>
<%
if(returnp3mtemp != null){
java.lang.String typeendpoint9 = returnp3mtemp.getEndpoint();
if(typeendpoint9 != null){
        String tempResultendpoint9 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typeendpoint9));
        %>
        <%= tempResultendpoint9 %>
        <%
}
else{
        %>
        <%= typeendpoint9%>
        <%
}
}
%>
</TD>
    </TABLE>
    <%
}
%>
<HR/><BR/>
<%
}
break;
case 13:
    gotMethod = true;
    if (methodKey != null) {
        javax.xml.ws.Response resp = com.ibm.ccl.ws.jaxws.gstc.util.AsyncUtils.getResponse(session, methodKey);
        if (resp != null) {
            isDone = resp.isDone();
            if (!isDone)
                break;
            if (resp.get() != null) {
                Object o = resp.get();
                if (o instanceof javax.xml.transform.Source)
                    sourceOut = (javax.xml.transform.Source) o;
            }
        }
    }
    else if (bypass) {
        javax.xml.ws.Dispatch dispatch = samplePractWS_CRUDPortProxyid._getDescriptor().getDispatch();

        if (request.getParameter("__use_soapaction__") != null)
            com.ibm.ccl.ws.jaxws.gstc.util.DispatchUtils.useSoapAction(dispatch, request.getParameter("__soapaction__"));
        else
            com.ibm.ccl.ws.jaxws.gstc.util.DispatchUtils.clearSoapAction(dispatch);

        if (!async)
            sourceOut = (javax.xml.transform.Source) dispatch.invoke(sourceIn);
        else {
            // async code omitted
            break;
        }
    } else {
        String arg0_0id=  request.getParameter("arg016");
        int arg0_0idTemp  = Integer.parseInt(arg0_0id);

        String arg1_2id=  request.getParameter("arg118");
        String arg118null = request.getParameter("arg118null");
        java.lang.String arg1_2idTemp;
        if (arg118null != null)
            arg1_2idTemp = null;
        else {
         arg1_2idTemp  = arg1_2id;
        }

        String arg2_4id=  request.getParameter("arg220");
        String arg220null = request.getParameter("arg220null");
        java.lang.String arg2_4idTemp;
        if (arg220null != null)
            arg2_4idTemp = null;
        else {
         arg2_4idTemp  = arg2_4id;
        }

        String arg3_6id=  request.getParameter("arg322");
        int arg3_6idTemp  = Integer.parseInt(arg3_6id);

        String arg4_8id=  request.getParameter("arg424");
        String arg424null = request.getParameter("arg424null");
        java.lang.String arg4_8idTemp;
        if (arg424null != null)
            arg4_8idTemp = null;
        else {
         arg4_8idTemp  = arg4_8id;
        }

        String arg5_10id=  request.getParameter("arg526");
        String arg526null = request.getParameter("arg526null");
        java.lang.String arg5_10idTemp;
        if (arg526null != null)
            arg5_10idTemp = null;
        else {
         arg5_10idTemp  = arg5_10id;
        }

        if (!async) {
        try {
            samplePractWS_CRUDPortProxyid.agrega(arg0_0idTemp,arg1_2idTemp,arg2_4idTemp,arg3_6idTemp,arg4_8idTemp,arg5_10idTemp);
            }catch(com.mx.everis.webservice.ClassNotFoundException_Exception ClassNotFoundException_Exception28){
            com.mx.everis.webservice.ClassNotFoundException ClassNotFoundException29 = ClassNotFoundException_Exception28.getFaultInfo();
                %>
<TABLE CLASS="tableform">
<TR>
<TD COLSPAN="3" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">com.mx.everis.webservice.ClassNotFoundException_Exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">stackTrace:</TD>
<TD>
<%
if(ClassNotFoundException29 != null){
com.mx.everis.webservice.Throwable tebece0=ClassNotFoundException29.getException();
if(tebece0 != null){
java.util.List typestackTrace32 = tebece0.getStackTrace();
if(typestackTrace32 != null){
        String tempResultstackTrace32 = com.ibm.ccl.ws.jaxws.gstc.util.Introspector.visit(typestackTrace32);
        %>
        <%= tempResultstackTrace32 %>
        <%
}
else{
        %>
        <%= typestackTrace32%>
        <%
}
}}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">message:</TD>
<TD>
<%
if(ClassNotFoundException29 != null){
java.lang.String typemessage34 = ClassNotFoundException29.getMessage();
if(typemessage34 != null){
        String tempResultmessage34 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typemessage34));
        %>
        <%= tempResultmessage34 %>
        <%
}
else{
        %>
        <%= typemessage34%>
        <%
}
}
%>
</TD>
</TABLE>
                <%
break;
            }catch(com.mx.everis.webservice.SQLException_Exception SQLException_Exception36){
            com.mx.everis.webservice.SQLException SQLException37 = SQLException_Exception36.getFaultInfo();
                %>
<TABLE CLASS="tableform">
<TR>
<TD COLSPAN="3" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">com.mx.everis.webservice.SQLException_Exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">sQLState:</TD>
<TD>
<%
if(SQLException37 != null){
java.lang.String typesQLState38 = SQLException37.getSQLState();
if(typesQLState38 != null){
        String tempResultsQLState38 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typesQLState38));
        %>
        <%= tempResultsQLState38 %>
        <%
}
else{
        %>
        <%= typesQLState38%>
        <%
}
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">errorCode:</TD>
<TD>
<%
if(SQLException37 != null){
int typeerrorCode40 = SQLException37.getErrorCode();
        String tempResulterrorCode40 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typeerrorCode40));
        %>
        <%= tempResulterrorCode40 %>
        <%
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">message:</TD>
<TD>
<%
if(SQLException37 != null){
java.lang.String typemessage42 = SQLException37.getMessage();
if(typemessage42 != null){
        String tempResultmessage42 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typemessage42));
        %>
        <%= tempResultmessage42 %>
        <%
}
else{
        %>
        <%= typemessage42%>
        <%
}
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">nextException:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">nextException:</TD>
<TD>
<%
if(SQLException37 != null){
com.mx.everis.webservice.SqlException tebece0=SQLException37.getNextException();
if(tebece0 != null){
com.mx.everis.webservice.SqlException typenextException46 = tebece0.getNextException();
if(typenextException46 != null){
        if(typenextException46!= null){
        String tempnextException46 = typenextException46.toString();
        %>
        <%=tempnextException46%>
        <%
        }}
else{
        %>
        <%= typenextException46%>
        <%
}
}}
%>
</TD>
</TABLE>
                <%
break;
            }catch(Exception exc){
                %>
                Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(exc.toString()) %>
                Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(exc.getMessage()) %>
                <%
break;
            }
        }
        else {
            // async code omitted
            break;
        }
    }
if (sourceOut != null) {
%>
    <TEXTAREA ROWS="8" COLs="45"><%= org.eclipse.jst.ws.util.JspUtils.markup(com.ibm.ccl.ws.jaxws.gstc.util.SourceUtils.transform(sourceOut)) %></TEXTAREA>
<%
}
else {
}
break;
case 48:
    gotMethod = true;
    if (methodKey != null) {
        javax.xml.ws.Response resp = com.ibm.ccl.ws.jaxws.gstc.util.AsyncUtils.getResponse(session, methodKey);
        if (resp != null) {
            isDone = resp.isDone();
            if (!isDone)
                break;
            if (resp.get() != null) {
                Object o = resp.get();
                if (o instanceof javax.xml.transform.Source)
                    sourceOut = (javax.xml.transform.Source) o;
            }
        }
    }
    else if (bypass) {
        javax.xml.ws.Dispatch dispatch = samplePractWS_CRUDPortProxyid._getDescriptor().getDispatch();

        if (request.getParameter("__use_soapaction__") != null)
            com.ibm.ccl.ws.jaxws.gstc.util.DispatchUtils.useSoapAction(dispatch, request.getParameter("__soapaction__"));
        else
            com.ibm.ccl.ws.jaxws.gstc.util.DispatchUtils.clearSoapAction(dispatch);

        if (!async)
            sourceOut = (javax.xml.transform.Source) dispatch.invoke(sourceIn);
        else {
            // async code omitted
            break;
        }
    } else {
        String arg0_12id=  request.getParameter("arg051");
        int arg0_12idTemp  = Integer.parseInt(arg0_12id);

        String arg1_14id=  request.getParameter("arg153");
        String arg153null = request.getParameter("arg153null");
        java.lang.String arg1_14idTemp;
        if (arg153null != null)
            arg1_14idTemp = null;
        else {
         arg1_14idTemp  = arg1_14id;
        }

        if (!async) {
        try {
            samplePractWS_CRUDPortProxyid.elimina(arg0_12idTemp,arg1_14idTemp);
            }catch(com.mx.everis.webservice.ClassNotFoundException_Exception ClassNotFoundException_Exception55){
            com.mx.everis.webservice.ClassNotFoundException ClassNotFoundException56 = ClassNotFoundException_Exception55.getFaultInfo();
                %>
<TABLE CLASS="tableform">
<TR>
<TD COLSPAN="3" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">com.mx.everis.webservice.ClassNotFoundException_Exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">stackTrace:</TD>
<TD>
<%
if(ClassNotFoundException56 != null){
com.mx.everis.webservice.Throwable tebece0=ClassNotFoundException56.getException();
if(tebece0 != null){
java.util.List typestackTrace59 = tebece0.getStackTrace();
if(typestackTrace59 != null){
        String tempResultstackTrace59 = com.ibm.ccl.ws.jaxws.gstc.util.Introspector.visit(typestackTrace59);
        %>
        <%= tempResultstackTrace59 %>
        <%
}
else{
        %>
        <%= typestackTrace59%>
        <%
}
}}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">message:</TD>
<TD>
<%
if(ClassNotFoundException56 != null){
java.lang.String typemessage61 = ClassNotFoundException56.getMessage();
if(typemessage61 != null){
        String tempResultmessage61 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typemessage61));
        %>
        <%= tempResultmessage61 %>
        <%
}
else{
        %>
        <%= typemessage61%>
        <%
}
}
%>
</TD>
</TABLE>
                <%
break;
            }catch(com.mx.everis.webservice.SQLException_Exception SQLException_Exception63){
            com.mx.everis.webservice.SQLException SQLException64 = SQLException_Exception63.getFaultInfo();
                %>
<TABLE CLASS="tableform">
<TR>
<TD COLSPAN="3" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">com.mx.everis.webservice.SQLException_Exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">sQLState:</TD>
<TD>
<%
if(SQLException64 != null){
java.lang.String typesQLState65 = SQLException64.getSQLState();
if(typesQLState65 != null){
        String tempResultsQLState65 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typesQLState65));
        %>
        <%= tempResultsQLState65 %>
        <%
}
else{
        %>
        <%= typesQLState65%>
        <%
}
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">errorCode:</TD>
<TD>
<%
if(SQLException64 != null){
int typeerrorCode67 = SQLException64.getErrorCode();
        String tempResulterrorCode67 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typeerrorCode67));
        %>
        <%= tempResulterrorCode67 %>
        <%
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">message:</TD>
<TD>
<%
if(SQLException64 != null){
java.lang.String typemessage69 = SQLException64.getMessage();
if(typemessage69 != null){
        String tempResultmessage69 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typemessage69));
        %>
        <%= tempResultmessage69 %>
        <%
}
else{
        %>
        <%= typemessage69%>
        <%
}
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">nextException:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">nextException:</TD>
<TD>
<%
if(SQLException64 != null){
com.mx.everis.webservice.SqlException tebece0=SQLException64.getNextException();
if(tebece0 != null){
com.mx.everis.webservice.SqlException typenextException73 = tebece0.getNextException();
if(typenextException73 != null){
        if(typenextException73!= null){
        String tempnextException73 = typenextException73.toString();
        %>
        <%=tempnextException73%>
        <%
        }}
else{
        %>
        <%= typenextException73%>
        <%
}
}}
%>
</TD>
</TABLE>
                <%
break;
            }catch(Exception exc){
                %>
                Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(exc.toString()) %>
                Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(exc.getMessage()) %>
                <%
break;
            }
        }
        else {
            // async code omitted
            break;
        }
    }
if (sourceOut != null) {
%>
    <TEXTAREA ROWS="8" COLs="45"><%= org.eclipse.jst.ws.util.JspUtils.markup(com.ibm.ccl.ws.jaxws.gstc.util.SourceUtils.transform(sourceOut)) %></TEXTAREA>
<%
}
else {
}
break;
case 75:
    gotMethod = true;
    if (methodKey != null) {
        javax.xml.ws.Response resp = com.ibm.ccl.ws.jaxws.gstc.util.AsyncUtils.getResponse(session, methodKey);
        if (resp != null) {
            isDone = resp.isDone();
            if (!isDone)
                break;
            if (resp.get() != null) {
                Object o = resp.get();
                if (o instanceof javax.xml.transform.Source)
                    sourceOut = (javax.xml.transform.Source) o;
            }
        }
    }
    else if (bypass) {
        javax.xml.ws.Dispatch dispatch = samplePractWS_CRUDPortProxyid._getDescriptor().getDispatch();

        if (request.getParameter("__use_soapaction__") != null)
            com.ibm.ccl.ws.jaxws.gstc.util.DispatchUtils.useSoapAction(dispatch, request.getParameter("__soapaction__"));
        else
            com.ibm.ccl.ws.jaxws.gstc.util.DispatchUtils.clearSoapAction(dispatch);

        if (!async)
            sourceOut = (javax.xml.transform.Source) dispatch.invoke(sourceIn);
        else {
            // async code omitted
            break;
        }
    } else {
        String arg0_16id=  request.getParameter("arg078");
        int arg0_16idTemp  = Integer.parseInt(arg0_16id);

        String arg1_18id=  request.getParameter("arg180");
        String arg180null = request.getParameter("arg180null");
        java.lang.String arg1_18idTemp;
        if (arg180null != null)
            arg1_18idTemp = null;
        else {
         arg1_18idTemp  = arg1_18id;
        }

        String arg2_20id=  request.getParameter("arg282");
        String arg282null = request.getParameter("arg282null");
        java.lang.String arg2_20idTemp;
        if (arg282null != null)
            arg2_20idTemp = null;
        else {
         arg2_20idTemp  = arg2_20id;
        }

        String arg3_22id=  request.getParameter("arg384");
        int arg3_22idTemp  = Integer.parseInt(arg3_22id);

        String arg4_24id=  request.getParameter("arg486");
        String arg486null = request.getParameter("arg486null");
        java.lang.String arg4_24idTemp;
        if (arg486null != null)
            arg4_24idTemp = null;
        else {
         arg4_24idTemp  = arg4_24id;
        }

        String arg5_26id=  request.getParameter("arg588");
        String arg588null = request.getParameter("arg588null");
        java.lang.String arg5_26idTemp;
        if (arg588null != null)
            arg5_26idTemp = null;
        else {
         arg5_26idTemp  = arg5_26id;
        }

        if (!async) {
        try {
            samplePractWS_CRUDPortProxyid.modifica(arg0_16idTemp,arg1_18idTemp,arg2_20idTemp,arg3_22idTemp,arg4_24idTemp,arg5_26idTemp);
            }catch(com.mx.everis.webservice.ClassNotFoundException_Exception ClassNotFoundException_Exception90){
            com.mx.everis.webservice.ClassNotFoundException ClassNotFoundException91 = ClassNotFoundException_Exception90.getFaultInfo();
                %>
<TABLE CLASS="tableform">
<TR>
<TD COLSPAN="3" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">com.mx.everis.webservice.ClassNotFoundException_Exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">stackTrace:</TD>
<TD>
<%
if(ClassNotFoundException91 != null){
com.mx.everis.webservice.Throwable tebece0=ClassNotFoundException91.getException();
if(tebece0 != null){
java.util.List typestackTrace94 = tebece0.getStackTrace();
if(typestackTrace94 != null){
        String tempResultstackTrace94 = com.ibm.ccl.ws.jaxws.gstc.util.Introspector.visit(typestackTrace94);
        %>
        <%= tempResultstackTrace94 %>
        <%
}
else{
        %>
        <%= typestackTrace94%>
        <%
}
}}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">message:</TD>
<TD>
<%
if(ClassNotFoundException91 != null){
java.lang.String typemessage96 = ClassNotFoundException91.getMessage();
if(typemessage96 != null){
        String tempResultmessage96 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typemessage96));
        %>
        <%= tempResultmessage96 %>
        <%
}
else{
        %>
        <%= typemessage96%>
        <%
}
}
%>
</TD>
</TABLE>
                <%
break;
            }catch(com.mx.everis.webservice.SQLException_Exception SQLException_Exception98){
            com.mx.everis.webservice.SQLException SQLException99 = SQLException_Exception98.getFaultInfo();
                %>
<TABLE CLASS="tableform">
<TR>
<TD COLSPAN="3" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">com.mx.everis.webservice.SQLException_Exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">sQLState:</TD>
<TD>
<%
if(SQLException99 != null){
java.lang.String typesQLState100 = SQLException99.getSQLState();
if(typesQLState100 != null){
        String tempResultsQLState100 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typesQLState100));
        %>
        <%= tempResultsQLState100 %>
        <%
}
else{
        %>
        <%= typesQLState100%>
        <%
}
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">errorCode:</TD>
<TD>
<%
if(SQLException99 != null){
int typeerrorCode102 = SQLException99.getErrorCode();
        String tempResulterrorCode102 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typeerrorCode102));
        %>
        <%= tempResulterrorCode102 %>
        <%
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">message:</TD>
<TD>
<%
if(SQLException99 != null){
java.lang.String typemessage104 = SQLException99.getMessage();
if(typemessage104 != null){
        String tempResultmessage104 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typemessage104));
        %>
        <%= tempResultmessage104 %>
        <%
}
else{
        %>
        <%= typemessage104%>
        <%
}
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">nextException:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">nextException:</TD>
<TD>
<%
if(SQLException99 != null){
com.mx.everis.webservice.SqlException tebece0=SQLException99.getNextException();
if(tebece0 != null){
com.mx.everis.webservice.SqlException typenextException108 = tebece0.getNextException();
if(typenextException108 != null){
        if(typenextException108!= null){
        String tempnextException108 = typenextException108.toString();
        %>
        <%=tempnextException108%>
        <%
        }}
else{
        %>
        <%= typenextException108%>
        <%
}
}}
%>
</TD>
</TABLE>
                <%
break;
            }catch(Exception exc){
                %>
                Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(exc.toString()) %>
                Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(exc.getMessage()) %>
                <%
break;
            }
        }
        else {
            // async code omitted
            break;
        }
    }
if (sourceOut != null) {
%>
    <TEXTAREA ROWS="8" COLs="45"><%= org.eclipse.jst.ws.util.JspUtils.markup(com.ibm.ccl.ws.jaxws.gstc.util.SourceUtils.transform(sourceOut)) %></TEXTAREA>
<%
}
else {
}
break;
case 110:
    gotMethod = true;
    java.util.List returnp111mtemp = null;
    if (methodKey != null) {
        javax.xml.ws.Response resp = com.ibm.ccl.ws.jaxws.gstc.util.AsyncUtils.getResponse(session, methodKey);
        if (resp != null) {
            isDone = resp.isDone();
            if (!isDone)
                break;
            if (resp.get() != null) {
                Object o = resp.get();
                if (o instanceof javax.xml.transform.Source)
                    sourceOut = (javax.xml.transform.Source) o;
                else if (o instanceof java.util.List)
                    returnp111mtemp = (java.util.List) o;
            }
        }
    }
    else if (bypass) {
        javax.xml.ws.Dispatch dispatch = samplePractWS_CRUDPortProxyid._getDescriptor().getDispatch();

        if (request.getParameter("__use_soapaction__") != null)
            com.ibm.ccl.ws.jaxws.gstc.util.DispatchUtils.useSoapAction(dispatch, request.getParameter("__soapaction__"));
        else
            com.ibm.ccl.ws.jaxws.gstc.util.DispatchUtils.clearSoapAction(dispatch);

        if (!async)
            sourceOut = (javax.xml.transform.Source) dispatch.invoke(sourceIn);
        else {
            // async code omitted
            break;
        }
    } else {
        String arg0_28id=  request.getParameter("arg0113");
        int arg0_28idTemp  = Integer.parseInt(arg0_28id);

        if (!async) {
        try {
            returnp111mtemp = samplePractWS_CRUDPortProxyid.consulta(arg0_28idTemp);
            }catch(com.mx.everis.webservice.ClassNotFoundException_Exception ClassNotFoundException_Exception115){
            com.mx.everis.webservice.ClassNotFoundException ClassNotFoundException116 = ClassNotFoundException_Exception115.getFaultInfo();
                %>
<TABLE CLASS="tableform">
<TR>
<TD COLSPAN="3" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">com.mx.everis.webservice.ClassNotFoundException_Exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">stackTrace:</TD>
<TD>
<%
if(ClassNotFoundException116 != null){
com.mx.everis.webservice.Throwable tebece0=ClassNotFoundException116.getException();
if(tebece0 != null){
java.util.List typestackTrace119 = tebece0.getStackTrace();
if(typestackTrace119 != null){
        String tempResultstackTrace119 = com.ibm.ccl.ws.jaxws.gstc.util.Introspector.visit(typestackTrace119);
        %>
        <%= tempResultstackTrace119 %>
        <%
}
else{
        %>
        <%= typestackTrace119%>
        <%
}
}}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">message:</TD>
<TD>
<%
if(ClassNotFoundException116 != null){
java.lang.String typemessage121 = ClassNotFoundException116.getMessage();
if(typemessage121 != null){
        String tempResultmessage121 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typemessage121));
        %>
        <%= tempResultmessage121 %>
        <%
}
else{
        %>
        <%= typemessage121%>
        <%
}
}
%>
</TD>
</TABLE>
                <%
break;
            }catch(com.mx.everis.webservice.SQLException_Exception SQLException_Exception123){
            com.mx.everis.webservice.SQLException SQLException124 = SQLException_Exception123.getFaultInfo();
                %>
<TABLE CLASS="tableform">
<TR>
<TD COLSPAN="3" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">com.mx.everis.webservice.SQLException_Exception:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">sQLState:</TD>
<TD>
<%
if(SQLException124 != null){
java.lang.String typesQLState125 = SQLException124.getSQLState();
if(typesQLState125 != null){
        String tempResultsQLState125 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typesQLState125));
        %>
        <%= tempResultsQLState125 %>
        <%
}
else{
        %>
        <%= typesQLState125%>
        <%
}
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">errorCode:</TD>
<TD>
<%
if(SQLException124 != null){
int typeerrorCode127 = SQLException124.getErrorCode();
        String tempResulterrorCode127 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typeerrorCode127));
        %>
        <%= tempResulterrorCode127 %>
        <%
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">message:</TD>
<TD>
<%
if(SQLException124 != null){
java.lang.String typemessage129 = SQLException124.getMessage();
if(typemessage129 != null){
        String tempResultmessage129 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typemessage129));
        %>
        <%= tempResultmessage129 %>
        <%
}
else{
        %>
        <%= typemessage129%>
        <%
}
}
%>
</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="2" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">nextException:</TD>
<TR>
<TD CLASS="spacercol">&nbsp;</TD>
<TD CLASS="spacercol">&nbsp;</TD>
<TD COLSPAN="1" VALIGN="TOP" ALIGN="LEFT" CLASS="headingcol">nextException:</TD>
<TD>
<%
if(SQLException124 != null){
com.mx.everis.webservice.SqlException tebece0=SQLException124.getNextException();
if(tebece0 != null){
com.mx.everis.webservice.SqlException typenextException133 = tebece0.getNextException();
if(typenextException133 != null){
        if(typenextException133!= null){
        String tempnextException133 = typenextException133.toString();
        %>
        <%=tempnextException133%>
        <%
        }}
else{
        %>
        <%= typenextException133%>
        <%
}
}}
%>
</TD>
</TABLE>
                <%
break;
            }catch(Exception exc){
                %>
                Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(exc.toString()) %>
                Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(exc.getMessage()) %>
                <%
break;
            }
        }
        else {
            // async code omitted
            break;
        }
    }
if (sourceOut != null) {
%>
    <TEXTAREA ROWS="8" COLs="45"><%= org.eclipse.jst.ws.util.JspUtils.markup(com.ibm.ccl.ws.jaxws.gstc.util.SourceUtils.transform(sourceOut)) %></TEXTAREA>
<%
}
else {
if (returnp111mtemp == null) {
%>
    null
<%
} else {
        String tempResultreturnp111 = com.ibm.ccl.ws.jaxws.gstc.util.Introspector.visit(returnp111mtemp);
        %>
        <%= tempResultreturnp111 %>
        <%
}
%>
<HR/><BR/>
<%
}
break;
}
} catch (Exception e) { 
%>
exception: <%=org.eclipse.jst.ws.util.JspUtils.markup(e.toString())%>
<%
return;
}
if(!gotMethod){
%>
Result: N/A
<%
} else if (!isDone) {
%>
No results available yet.
<%
} else if (async && methodKey == null) {
%>
The service has been invoked.
<script language="JavaScript">reloadMethods();</script>
<%
}
%>
</BODY>
</HTML>